  $(document).ready(function(){
    $(".owl-carousel").owlCarousel({
        items : 1, 
        navContainer : ".nav-container",
        dotsContainer : ".owl-dots",
        loop : true,
    });
  });
      


const initialize = () => {
    const toggleClass = (elements, className, elementIndex,  isRemove = true) => {
        elements.forEach((item, index) => {
            if (isRemove) {
                if (elementIndex !== index) {
                    item.classList.remove(className)
                }
            } else {
                if (elementIndex !== index) {
                    item.classList.add(className)
                }
            }
        })
    }

 
    const hidden = () => {
        
        const btns = document.querySelectorAll('.work-list-btn')

        const serviceBtn = document.querySelector(".btn-work")
        const workList = document.querySelector(".work-list")
        const workListItem = document.getElementsByClassName('work-list-item')
        
        serviceBtn.addEventListener('click', () =>{
            for (let i =  1; i < 13; i++) {
                const newElement = document.createElement("li")
                newElement.classList.add("work-list-item")
                newElement.dataset.theme = "graphic-design"
                newElement.insertAdjacentHTML("afterbegin", `
                <${i}mg src="step-project1/img/graphic-design/graphic-designi.jpg" alt="work-photo" class="img-work">
                <div class="hidden-block">
                            <div class="image-hidden-block-wrapper">
                                <img class ='image-hidden-block-scrap' src="img/logo/scrap.svg" alt="#">
                                <div class="circle-figure"><div class="square-figure"></div></div>
                            </div>
                            <p class="tittle-hidden-block">creative design</p>
                            <p class="new-subtittle-hidden-block">Graphic Design</p>
                        </div>`)
                        workList.append(newElement)

                        if(i > 3 && i < 7){
                            newElement.dataset.theme = "web-design"
                            newElement.innerHTML = newElement.innerHTML.replace("Graphic Design", "Web Design")
                            }
                            if(i > 6 && i < 10){
                                newElement.dataset.theme = "landing-pages"
                                newElement.innerHTML = newElement.innerHTML.replace("Graphic Design", "Landing Pages")
                            }
                            if(i > 9 && i < 13){
                                newElement.dataset.theme = "wordpress"
                                newElement.innerHTML = newElement.innerHTML.replace("Graphic Design", "Wordpress")
                            }
                        
            }
            serviceBtn.style.visibility = "hidden"
            btns.forEach((item) => {
                if ( item.classList.contains("border-active")){
                    let items = document.querySelectorAll('.work-list-item')
                    items.forEach(itemPicture => {
                        const btnTheme = item.dataset.themebtn
                        if (btnTheme === 'all'  ) {
                            itemPicture.classList.remove('work-list-themed-hidden')
                            item.classList.add('border-active')
                            return
                        }
                        if (btnTheme !== itemPicture.dataset.theme) {
                            itemPicture.classList.add('work-list-themed-hidden')
                            item.classList.add('border-active')
                        } else {
                            itemPicture.classList.remove('work-list-themed-hidden')
                            item.classList.add('border-active')
                      }
                    })
                }
            })
        })
    }
    

    const handleSectionsForService = () => {
        const servicesButtons = document.querySelectorAll('.list-item-service')
        const serviceElements = document.querySelectorAll('.our-services-photo')
        const texts = document.querySelectorAll('.our-services-text-content')
            servicesButtons.forEach((item, index) => {
                item.addEventListener('click', () => {
                    serviceElements[index].classList.remove('our-services-photo-hidden')  
                    item.classList.add('our-service-background-active')
                    texts[index].classList.remove('our-services-text-hidden')

                    toggleClass(texts, 'our-services-text-hidden', index, false)

                    toggleClass(servicesButtons, 'our-service-background-active',  index)

                    toggleClass(serviceElements, 'our-services-photo-hidden', index, false)
                    
                })
            })
    }
   

    const showTheme = () => {
        let items = document.getElementsByClassName('work-list-item')
        const btns = document.querySelectorAll('.work-list-btn')
        const serviceButton = document.querySelector('.btn-work')
        const primaryBtn = document.querySelector('.work-list-btn')
        
        btns.forEach((item, index)  => {
            const btnTheme = item.dataset.themebtn
            item.addEventListener('click', () =>{
            for (const itemList of items) {
              [itemList].forEach(itemPicture => {
                console.log(btnTheme)
                if (btnTheme === 'all'  ) {
                    itemPicture.classList.remove('work-list-themed-hidden')
                    item.classList.add('border-active')
                    return
                }
                if (btnTheme !== itemPicture.dataset.theme) {
                    itemPicture.classList.add('work-list-themed-hidden')
                    item.classList.add('border-active')
                } else {
                    itemPicture.classList.remove('work-list-themed-hidden')
                    item.classList.add('border-active')
              }
            })
            }
                toggleClass(btns, 'border-active',  index)
        }
            )
        })
    }



   
    showTheme()
    hidden()
    handleSectionsForService()
}

initialize();


